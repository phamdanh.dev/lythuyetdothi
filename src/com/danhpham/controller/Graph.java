package com.danhpham.controller;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

public abstract class Graph {

	int[][] matrix;

	int[] visitedLienThong;

	public Graph() {

	}

	public Graph(String txtPath) {
		this.importTxtFile(txtPath);
	}

	// IMPORT MA TRAN TU FILE .TXT
	public boolean importTxtFile(String txtPath) {

		try {
			// BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(txtPath),
			// Charset.forName("UTF-8"));
			RandomAccessFile accessFile = new RandomAccessFile(txtPath, "r");
			String line = accessFile.readLine();
			int size = Integer.parseInt(line);
			this.matrix = new int[size][size];
			int count = 0;
			String lines = null;
			while ((lines = accessFile.readLine()) != null) {
				String[] linesArray = lines.split(" ");
				for (int i = 0; i < matrix.length; i++) {
					matrix[count][i] = Integer.parseInt(linesArray[i]);
				}
				count++;
			}
			accessFile.close();

		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	// IN RA MA TRAN KE
	public void printMatrix() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				System.out.print(matrix[i][j] + " ");
			}
			System.out.println();
		}
		System.out.println();
	}

	// DEM SO DINH DO THI
	public int countVertex() {
		return matrix.length;
	}

	// KIEM TRA 2 CANH CO KE NHAU
	public boolean checkKeNhau(int u, int v) {
		boolean check = false;
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[u][i] == 1) {
				if (i == v) {
					check = true;
					return check;
				}
			}
		}
		return check;
	}

	// IN RA CAC CANH CUA DO THI
	public void printEdgeGraph() {
		System.out.println("CAC CANH CUA DO THI: ");
		for (int i = 0; i < matrix.length; i++) {
			for (int j = i; j < matrix.length; j++) {
				if (matrix[i][j] == 1) {
					System.out.print("[" + i + "," + j + "]");
				}

			}
		}
		System.out.println();
	}

	// THEM CANH
	public abstract boolean addEdge(int u, int v);

	// BOT CANH
	public abstract boolean deleteEdge(int u, int v);

	// TINH SO CANH DO THI
	public abstract int countEdge();

	// DUYET THEO DFS
	public void doDFS(int i) {
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listDFS = new ArrayList<Integer>();
		int[] visited = new int[this.countVertex()];
		stack.push(i);
		listDFS.add(i);
		visited[i] = 1;
		while (!stack.empty()) {
			i = stack.peek();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == 1 && visited[j] == 0) {
					stack.push(j);
					visited[j] = 1;
					listDFS.add(j);
					break;
				}
				if (j == (matrix.length - 1)) {
					stack.pop();
					break;
				}
			}

		}

		System.out.print("DUYET DO THI THEO DFS: ");
		for (Integer item : listDFS) {
			System.out.print(item + " ");
		}
		System.out.println();
	}

	// DFS CHO MUC DICH COUNT LIEN THONG
	private void doDFSLienThong(int i) {
		Stack<Integer> stack = new Stack<Integer>();
		int[] visitedLienThong = new int[this.countVertex()];
		stack.push(i);
		visitedLienThong[i] = 1;
		while (!stack.empty()) {
			i = stack.peek();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == 1 && visitedLienThong[j] == 0) {
					stack.push(j);
					visitedLienThong[j] = 1;
					break;
				}
				if (j == (matrix.length - 1)) {
					stack.pop();
					break;
				}
			}

		}
	}

	// DUYET THEO BFS
	public void doBFS(int i) {
		Queue<Integer> queue = new LinkedList<Integer>();
		ArrayList<Integer> listBFS = new ArrayList<Integer>();
		int[] visited = new int[matrix.length];
		visited[i] = 1;
		queue.add(i);
		listBFS.add(i);
		while (!queue.isEmpty()) {
			boolean check = false;
			i = queue.peek();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == 1 && visited[j] == 0) {
					queue.add(j);
					visited[j] = 1;
					listBFS.add(j);
					check = true;
				}

			}
			if (check == false) {
				queue.remove();
			}
		}
		System.out.print("DUYET DO THI THEO BFS: ");
		for (Integer item : listBFS) {
			System.out.print(item + " ");
		}
		System.out.println();

	}

	// DEM BAC CUA DINH
	public abstract int countDegreeVertex(int i);

	// DEM TONG BAC CUA DO THI
	public abstract int countDegreeGraph();

	// KIEM TRA DO THI CO LIEN THONG
	public boolean kiemTraLienThong() {
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listDFS = new ArrayList<Integer>();
		int[] visited = new int[this.countVertex()];
		int i = 0;
		stack.push(i);
		listDFS.add(i);
		visited[i] = 1;
		while (!stack.empty()) {
			i = stack.peek();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == 1 && visited[j] == 0) {
					stack.push(j);
					visited[j] = 1;
					listDFS.add(j);
					break;
				}
				if (j == (matrix.length - 1)) {
					stack.pop();
					break;
				}
			}

		}
		for (int j = 0; j < visited.length; j++) {
			if (visited[j] == 0) {
				System.out.println("DO THI KHONG LIEN THONG");
				return false;
			}
		}
		System.out.println("DO THI LIEN THONG");
		return true;
	}
	
	
	// TIM MOT THANH PHAN LIEN THONG TU 1 DINH CHO TRUOC
	public void timMotThanhPhanLienThong(int v) {
		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listDFS = new ArrayList<Integer>();
		int[] visited = new int[this.countVertex()];
		stack.push(v);
		listDFS.add(v);
		visited[v] = 1;
		while (!stack.empty()) {
			v = stack.peek();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[v][j] == 1 && visited[j] == 0) {
					stack.push(j);
					visited[j] = 1;
					listDFS.add(j);
					break;
				}
				if (j == (matrix.length - 1)) {
					stack.pop();
					break;
				}
			}

		}
		System.out.print("CAC DINH TRONG THANH PHAN LIEN THONG CUA DINH " + v + " : ");
		for (int i : listDFS) {
			System.out.print(i + " ");
		}
		System.out.println();
	}
	
	

	// KIEM TRA CO DUONG DI TU 2 DINH CHO TRUOC KHONG
	public boolean kiemTraDuongDi(int u, int v) {
		Stack<Integer> stack = new Stack<Integer>();
		int[] visited = new int[this.countVertex()];
		stack.push(u);
		visited[u] = 1;
		boolean check = false;
		while (!stack.empty()) {
			u = stack.peek();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[u][j] == 1 && visited[j] == 0) {
					if (j == v) {
						check = true;
						return check;
					}
					stack.push(j);
					visited[j] = 1;
					break;
				}
				if (j == (matrix.length - 1)) {
					stack.pop();
					break;
				}
			}
		}
		if (visited[v] == 0) {
			check = false;
		}
		return check;
	}

	// TIM MOT THANH PHAN LIEN THONG

	// DEM SO THANH PHAN LIEN THONG
	public int countSoThanhPhanLienThong() {
		if (visitedLienThong != null) {
			for (int i = 0; i < matrix.length; i++) {
				visitedLienThong[i] = 0;
			}
		} else {
			visitedLienThong = new int[countVertex()];
		}
		Stack<Integer> stack = new Stack<Integer>();
		int i = 0;
		stack.push(i);
		visitedLienThong[i] = 1;
		int count = 1;
		while (!stack.empty()) {
			i = stack.peek();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == 1 && visitedLienThong[j] == 0) {
					stack.push(j);
					visitedLienThong[j] = 1;
					break;
				}
				if (j == (matrix.length - 1)) {
					stack.pop();
					break;
				}
			}

		}
		for (int j = 0; j < visitedLienThong.length; j++) {
			if (visitedLienThong[j] == 0) {
				count++;
				doDFSLienThong(j);
				;
			}
		}
		System.out.println("SO THANH PHAN LIEN THONG: " + count);
		return count;
	}

	// IN DUONG DI SO CAP TU 2 DINH NEU CO
	public void inDuongDi(int u, int v) {
		final int f = u;
		if (kiemTraDuongDi(u, v) == false) {
			System.out.println("KHONG CO DUONG DI TU " + u + " DEN " + v);
			return;
		}

		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listDFS = new ArrayList<Integer>();
		int[] visited = new int[this.countVertex()];
		stack.push(u);
		listDFS.add(u);
		visited[u] = 1;
		while (!stack.empty()) {
			u = stack.peek();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[u][j] == 1 && visited[j] == 0) {

					stack.push(j);
					visited[j] = 1;
					listDFS.add(j);
					if (j == v) {
						System.out.print("DUONG DI TU " + f + " DEN " + v + " LA: ");
						for (Integer item : listDFS) {
							System.out.print(item + " ");
						}
						System.out.println();
						return;
					}
					break;
				}
				if (j == (matrix.length - 1)) {
					stack.pop();
					break;
				}
			}

		}

	}

	// KIEM TRA DO THI CO PHAI LA DO THI LUONG PHAN
	public boolean kiemTraLuongPhan() {
		boolean check = true;
		;
		if (kiemTraLienThong() == false) {
			return check = false;
		}

		Stack<Integer> stack = new Stack<Integer>();
		ArrayList<Integer> listDFS = new ArrayList<Integer>();
		int[] visited = new int[this.countVertex()];
		int i = 0;
		int[] color = new int[this.countVertex()];
		color[0] = 0;
		stack.push(i);
		listDFS.add(i);
		visited[i] = 1;
		while (!stack.empty()) {
			i = stack.peek();
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == 1 && visited[j] == 0) {
					if (color[i] == 0) {
						color[j] = 1;
					} else {
						color[j] = 0;
					}
					stack.push(j);
					visited[j] = 1;
					listDFS.add(j);
					break;
				}
				if (j == (matrix.length - 1)) {
					stack.pop();
					break;
				}
			}

		}
		for (int x = 0; x < matrix.length; x++) {
			for (int y = 0; y < matrix.length; y++) {
				if (checkKeNhau(x, y) == true) {
					if (color[x] == color[y]) {
						check = false;
					}
				}
			}
		}
		if (check == true) {
			System.out.println("DO THI LUONG PHAN");
		} else {
			System.out.println("DO THI KHONG LUONG PHAN");
		}
		return check;
	}
}
