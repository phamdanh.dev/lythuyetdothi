package com.danhpham.controller;

public class DirectedGraph extends Graph {

	public DirectedGraph() {

	}

	public DirectedGraph(String txtPath) {
		this.importTxtFile(txtPath);
	}

	//TINH NUA BAC TRONG
	public int nuaBacTrong(int v) {
		int result = 0;
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[i][v] == 1) {
				result++;
			}
		}
		return result;
	}

	//TINH NUA BAC NGOAI
	public int nuaBacNgoai(int v) {
		int result = 0;
		for (int i = 0; i < matrix.length; i++) {
			if (matrix[v][i] == 1) {
				result++;
			}
		}
		return result;
	}

	@Override
	//THEM CANH
	public boolean addEdge(int u, int v) {
		while (u > matrix.length && v > matrix.length && matrix[u][v] == 1 && matrix[v][u] == 1) {
			System.out.println("THEM CANH KHONG THANH CONG");
			return false;
		}
		matrix[u][v]++;
		System.out.println("THEM CANH ( " + u + ", " + v + " ) THANH CONG");
		return true;
	}

	@Override
	//XOA CANH
	public boolean deleteEdge(int u, int v) {
		while (u > matrix.length && v > matrix.length && matrix[u][v] == 0 && matrix[v][u] == 0) {
			System.out.println("XOA CANH KHONG THANH CONG");
			return false;
		}
		matrix[u][v]--;
		System.out.println("XOA CANH ( " + u + ", " + v + " ) THANH CONG");
		return true;
	}

	@Override
	//DEM SO CANH
	public int countEdge() {
		int result = 0;
		for (int i = 0; i < matrix.length; i++) {
			result += nuaBacNgoai(i);
		}
		return result;
	}

	@Override
	//DEM TONG SO BAC CUA DINH
	public int countDegreeVertex(int i) {
		int result = nuaBacTrong(i) + nuaBacNgoai(i);
		return result;
	}

	@Override
	//DEM TONG SO BAC CUA DO THI
	public int countDegreeGraph() {
		int result = 0;
		for (int i = 0; i < matrix.length; i++) {
			result += countDegreeVertex(i);
		}
		return result;
	}

}
