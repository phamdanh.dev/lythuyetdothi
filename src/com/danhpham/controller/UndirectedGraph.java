package com.danhpham.controller;

public class UndirectedGraph extends Graph {

	public UndirectedGraph() {
	}

	public UndirectedGraph(String txtPath) {
		this.importTxtFile(txtPath);
	}

	// THEM CANH MOI
	@Override
	public boolean addEdge(int u, int v) {
		while (u > matrix.length && v > matrix.length && matrix[u][v] == 1 && matrix[v][u] == 1) {
			System.out.println("THEM CANH KHONG THANH CONG");
			return false;
		}
		matrix[u][v]++;
		matrix[v][u]++;
		System.out.println("THEM CANH ( " + u + ", " + v + " ) THANH CONG");
		return true;
	}

	// XOA CANH HIEN CO
	@Override
	public boolean deleteEdge(int u, int v) {
		while (u > matrix.length && v > matrix.length && matrix[u][v] == 0 && matrix[v][u] == 0) {
			System.out.println("XOA CANH KHONG THANH CONG");
			return false;
		}
		matrix[u][v]--;
		matrix[v][u]--;
		System.out.println("XOA CANH ( " + u + ", " + v + " ) THANH CONG");
		return true;
	}

	// TINH SO CANH MA TRAN
	public int countEdge() {
		int number = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == 1) {
					number++;
				}
			}
		}
		return number / 2;
	}


	// TONG BAC CUA 1 DINH
	public int countDegreeVertex(int i) {
		int result = 0;
		for (int j = 0; j < matrix.length; j++) {
			if (matrix[i][j] == 1) {
				result++;
			}
		}
		return result;
	}

	// TONG BAC CUA DO THI
	public int countDegreeGraph() {
		int result = 0;
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix.length; j++) {
				if (matrix[i][j] == 1) {
					result++;
				}
			}
		}
		return result;
	}

}
