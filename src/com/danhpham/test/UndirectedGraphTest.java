package com.danhpham.test;

import com.danhpham.controller.Graph;
import com.danhpham.controller.UndirectedGraph;

public class UndirectedGraphTest {
	
	public static void main(String[] args) {
		
		//UNDIRECTED GRAPH
		Graph graph = new UndirectedGraph("FileTextGraph/UndirectedGraph.txt");
		graph.printMatrix();
		graph.addEdge(0, 0);
		graph.deleteEdge(0, 0);
		System.out.println("SO CANH CUA DO THI: " + graph.countEdge());
		System.out.println("SO DINH CUA DO THI: " + graph.countVertex());
		graph.doDFS(2);
		System.out.println("SO BAC CUA 1 DINH: " + graph.countDegreeVertex(3));
		System.out.println("TONG SO BAC CUA DO THI: " + graph.countDegreeGraph());
		graph.printEdgeGraph();
		graph.doBFS(2);
		graph.kiemTraLienThong();
		graph.timMotThanhPhanLienThong(4);
		graph.countSoThanhPhanLienThong();
		graph.inDuongDi(0, 4);
		graph.kiemTraLuongPhan();

	}
	
}
